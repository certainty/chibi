(use chibi test extras ports)

(define-syntax test-eval
  (syntax-rules ()
    ((_ result exp)
     (test (with-output-to-string (cut write exp))
           result
           (condition-case
               (let ((ctx (make-default-context)))
                 (sexp->chicken (eval exp ctx) ctx))
             (exn (chibi)
                  (with-output-to-port (current-error-port)
                    (lambda ()
                      (newline)
                      (display "Error:")
                      (newline)
                      (pp (condition->list exn))))))))))

(test-eval 1.0 1.0)
(test-eval 123 123)
(test-eval "foo" "foo")
(test-eval '() ''())
(test-eval 'foo ''foo)
(test-eval '(1 foo) '(list 1 'foo))
(test-eval 1 '(begin (newline) (display 'you-should-see-this) (newline) 1))

(test "evaling a chibi sexp"
      '(foo bar 1 2 3)
      (let ((ctx (make-default-context)))
        (sexp->chicken (eval (eval ''(list 'foo 'bar 1 2 3) ctx) ctx) ctx)))


(let ((o #f))
  (let ((ctx (make-default-context)))
    (set! o (eval "hey" ctx)))
  (gc #t)
  (test "hey" (sexp->chicken o)))
